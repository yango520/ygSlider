import Log  from "../../utils/log"
const lg = new Log('ygSlider组件'); // 可以传入一个值作为该页面的标识
export default {
    data(){
        return {
            version: '1.0.1',
            pct: 0,
            maxValue: 0,  // 最大刻度值
            startX: 0,  // 滑块在屏幕的x轴开始位置
            endX: 0,  // 滑块在屏幕的x轴结束位置
            sliderW: 0, // 滑块长度
            colorList: [],  //存放gColor组两色值间渐变值组
            nowColor: '#FFFFFF',
            isShowCard: false,
            scaleValue: 0,
        }
    },
    props: {
        // 渐变色值组，这里需要跟css样式里的一至。尝试在内联样式绑定linear-gradient，切报错提示无法绑定渐变的内联样式
        // 已提Issues：https://gitee.com/openharmony/docs/issues/I5F8KC?from=project-issue
        gColor: {
            type: Array,
            default: [],
        },
        //步长，每两渐变色之间步长，步长越大 越精准，渲染压力越大，适当即可。
        step: {
            type: Number,
            default: 20,
        },
        showCard: {
            type: Boolean,
            default: false,
        },
    },
    onInit() {
    },
    onPageShow(){
        this.setColorList();
    },
    // 生成渐变色值组
    setColorList(){
        for(let i = 0; i < this.gColor.length - 1; i++){
            let res = this.gradientColor(this.gColor[i], this.gColor[i+1], this.step)
            this.colorList.push(...res)
        }
        this.maxValue = this.colorList.length - 1;
        this.nowColor = this.colorList[0]
    },

    // 触摸开始
    sliderStart(){
        this.showCard ? this.isShowCard = true : this.isShowCard = false;
        const t = this.$refs.ygSlider.getBoundingClientRect();
        this.startX = t.left;
        this.endX = t.left + t.width;
        this.sliderW = t.width;
    },
    // 触摸移动
    sliderMove(e){
        let x = e.touches[0].globalX;
        let s_x = x - this.startX;
        if(x <= this.startX){
            this.pct = 0;
        } else if(x >= this.endX){
            this.pct = 100;
        } else {
            this.pct =  s_x * 100 / this.sliderW;
        }
        let r = this.getValue(s_x);
        this.scaleValue = r;
        this.nowColor = this.colorList[r];
//        this.$emit('getResult', {
//            num: r,
//            rgbColor: this.nowColor,
//            hexColor: this.rgbToHex(this.nowColor),
//            hex: this.colorToHex(this.nowColor),
//        })
    },

    // 触摸结束
    sliderEnd(e){
        this.isShowCard = false;
        let x = e.touches[0]?.globalX || e.changedTouches[0].globalX;
        lg.debug(x)
        let s_x = x - this.startX;
        let r = this.getValue(s_x);
        this.nowColor = this.colorList[r];
        this.$emit('getResult', {
            num: r,
            rgbColor: this.nowColor,
            hexColor: this.rgbToHex(this.nowColor),
            hex: this.colorToHex(this.nowColor),
        })
    },
    // 获取当前刻度值
    getValue(s_x){
        let r = Math.ceil(this.maxValue * s_x / this.sliderW);
        if(r <= 0){
            r = 0;
        } else if(r >= this.maxValue) {
            r = this.maxValue;
        }
        return r;
    },
    /**
     * @description: 封装颜色渐变之间值
     * @param {String} startColor 开始颜色hex
     * @param {Number} endColor 结束颜色hex
     * @param {Number} step 渐变精度
     * @return {Array}
     */
    gradientColor(startColor,endColor,step){
        let startRGB = this.hexToRgb(startColor);//转换为rgb数组模式
        let endRGB = this.hexToRgb(endColor);

        let sR = (endRGB[0]-startRGB[0])/step;//总差值
        let sG = (endRGB[1]-startRGB[1])/step;
        let sB = (endRGB[2]-startRGB[2])/step;
        var colorArr = [];
        for(var i=0;i<step;i++){
            //计算每一步的hex值
            var hex = `rgb(${parseInt((sR*i+startRGB[0]))},${parseInt((sG*i+startRGB[1]))},${parseInt((sB*i+startRGB[2]))})`;
            colorArr.push(hex);
        }
        return colorArr;
    },


    // 将hex表示方式转换为rgb表示方式(这里返回rgb数组模式)
    hexToRgb(sColor){
        var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
        sColor = sColor.toLowerCase();
        if(sColor && reg.test(sColor)){
            if(sColor.length === 4){
                var sColorNew = "#";
                for(var i=1; i<4; i+=1){
                    sColorNew += sColor.slice(i,i+1).concat(sColor.slice(i,i+1));
                }
                sColor = sColorNew;
            }
            //处理六位的颜色值
            var sColorChange = [];
            for(let i=1; i<7; i+=2){
                sColorChange.push(parseInt("0x"+sColor.slice(i,i+2)));
            }
            return sColorChange;
        }else{
            return sColor;
        }
    },

    // 将rgb表示方式转换为hex表示方式
    rgbToHex(rgb){
        var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
        if(/^(rgb|RGB)/.test(rgb)){
            let aColor = rgb.replace(/(?:\(|\)|rgb|RGB)*/g,"").split(",");
            let strHex = "#";
            for(let i=0; i<aColor.length; i++){
                let hex = this.getHexNumber(aColor[i]);
                if(hex === "0"){
                    hex += hex;
                }
                strHex += hex;
            }
            if(strHex.length !== 7){
                strHex = rgb;
            }
            return strHex;
        }else if(reg.test(rgb)){
            var aNum = rgb.replace(/#/,'').split('');
            if(aNum.length === 6){
                return rgb;
            }else if(aNum.length === 3){
                var numHex = "#";
                for(let i=0; i<aNum.length; i+=1){
                    numHex += (aNum[i]+aNum[i]);
                }
                return numHex;
            }
        }else{
            return rgb;
        }
    },

    //将hexColor或者rgbColor转换hex的RGB值对象
    colorToHex(rgb){
        if(rgb && rgb.indexOf('#') > -1){
            var aNum = rgb.replace(/#/,'').toUpperCase().split('');
            if(aNum.length === 6){
                return {
                    R: aNum[0] + aNum[1],
                    G: aNum[2] + aNum[3],
                    B: aNum[4] + aNum[5]
                };
            }else if(aNum.length === 3){
                return {
                    R: aNum[0] + aNum[0],
                    G: aNum[1] + aNum[1],
                    B: aNum[2] + aNum[2]
                };
            } else {
                return rgb;
            }
        }  else if(/^(rgb|RGB)/.test(rgb)){
            let aColor = rgb.replace(/(?:\(|\)|rgb|RGB)*/g,"").split(",");
            let R = this.getHexNumber(aColor[0]);
            let G = this.getHexNumber(aColor[1]);
            let B = this.getHexNumber(aColor[2]);
            return {R,G,B};
        } else {
            return rgb;
        }
    },
    getHexNumber(str){
        if(typeof str === 'string'){
            return Number(str).toString(16).padStart(2,'0').toUpperCase()
        } else {
            return str;
        }
    }
}
