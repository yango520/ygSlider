import prompt from '@system.prompt';
class Ohos {
  toast(message, duration = 2000, bottom){
    prompt.showToast({
      message,
      duration,
      bottom
    });
  }
}

export default new Ohos();