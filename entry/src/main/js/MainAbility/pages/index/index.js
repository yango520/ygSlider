import Log  from "../../common/utils/log"
const lg = new Log('index页面'); // 可以传入一个值作为该页面的标识
export default {
    data: {
        gColor: [
            '#FF0000',
            '#FFE300',
            '#74FF00',
            '#00FFA6',
            '#00FDFF',
            '#0034FC',
            '#4200FF',
            '#BC00FF',
            '#FD00FF',
            '#FF0000'
        ],
        step: 20,
        showCard: true,
    },
    onInit() {

    },
    getResult(res){
        lg.info(res._detail)
    },
}
